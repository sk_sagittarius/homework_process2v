﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HomeworkProcess2v
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //var t2 = Thread.CurrentThread.ManagedThreadId; // Ид
            //var t3 = Thread.CurrentThread.Name; // Имя
            //var t4 = Thread.CurrentThread.Priority; // Приоритет
            //var t5 = Thread.CurrentThread.; // Объем физ памяти workingset64
            var processesList = Process.GetProcesses().ToList();
            processDataGrid.ItemsSource = null;
            processDataGrid.ItemsSource = processesList;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(10);
            timer.Tick += TimerAlarm;

            timer.Start();
        }

        private void TimerAlarm(object sender, EventArgs e)
        {
            var processesList = Process.GetProcesses().ToList();
            processDataGrid.ItemsSource = null;
            processDataGrid.Items.Refresh();
            processDataGrid.ItemsSource = processesList;
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedProccess = (Process)processDataGrid.SelectedItem;
                var listProcessByName = Process.GetProcessesByName(selectedProccess.ProcessName); // много chrome
                foreach (var process in listProcessByName)
                {
                    process.Kill();
                }

                var processesList = Process.GetProcesses().ToList();
                processDataGrid.ItemsSource = null;
                processDataGrid.Items.Refresh();
                processDataGrid.ItemsSource = processesList;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Нет доступа");
                throw;
            }
        }
    }
}
